import { Component, ViewChild, ElementRef } from '@angular/core';
import { SearchParty } from './models/SearchParty';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // Add a reference to the new forname textbox
  @ViewChild('forenameInput') inputElement: ElementRef;

  title = 'Name List';
  searchParties: Array<SearchParty> = [];

  searchParty: SearchParty = new SearchParty();

  getSearchParties(): Array<SearchParty> {
    return this.searchParties;
  }

  addSearchParty(): void {
    const forenames = this.searchParty.forenames;
    const surname = this.searchParty.surname;

    if ((forenames && forenames.trim().length > 0) && (surname && surname.trim().length > 0)) {
      this.searchParties.push(this.searchParty);
      this.searchParty = new SearchParty();
      // Focus on the new forename textbox
      this.inputElement.nativeElement.focus();
    }
  }

  removeSearchParty(index: number) {
    this.searchParties.splice(index, 1);
    // Focus on the new forename textbox
    this.inputElement.nativeElement.focus();
  }
}
